
Rocky Build Process Overview
============


This is a simple document that explains going from RHEL sources to freshly (re)-built Rocky Linux packages.  It's not intended to be overly technical or specific at all, just an introduction (and general plan) to someone interested in how the process will work.


## The Steps: Going from RHEL 8 Source to Rocky 8 Binary Package
1.  Download the RHEL 8 source RPMs from their repositories
2.  Extract the source RPM contents into a git repository, in a branch marked for upstream, like "RHEL8-original" 
3.  Copy the source RPM contents to another git branch ("Rocky8-branch"), performing any de-branding / patching as necessary
4. Produce a Rocky 8 Source RPM from the "Rocky8" branch, likely using Koji/Mock RPM build tools
5. Compile the source RPM to a Rocky 8 binary RPM using the build tools
6. Sign and test the RPM in an automatic way
7. Deploy it to the Rocky Linux repository


Obviously, each of these steps has a lot more to it.  This document will not get in-depth about ways and means of accomplishing each step.  Obviously we want each of these to be as automated as possible.  We'll take a (short) look at each one in turn:

<br />

### Step 1: Downloading Source RPMs
This is fairly straightforward.  If you want to re-build RHEL 8, you need the source to RHEL 8.  Fortunately, it's fairly easy to get this via the ```dnf reposync``` command.  The sources for a rebuild involve reposync'ing 4 repositories:

```
rhel-8-for-x86_64-baseos-source-rpm
rhel-8-for-x86_64-supplementary-source-rpms
rhel-8-for-x86_64-appstream-source-rpms
codeready-builder-for-rhel-8-x86_64-source-rpms
```

Downloading the entirety of these repositories via yum/dnf should (in theory) give you all the source you need to re-create the entirety of RHEL 8.

Rocky Linux infrastructure should have 1-2 registered RHEL 8 machines who frequently check for new/updated packages from these source repositories.  New source RPM downloads can then be automatically copied/extracted in step 2.

<br />

### Step 2: Extract source RPMs
Each package in RHEL should have a corresponding Rocky Linux Git repository dedicated to it.  For example: Rocky Linux will have a bash repository, a python3 repository, a python3-gpg repository, etc.  **One git repo for each package.**  Yes, that's a lot of repos.

We take the contents of the source RPM, and extract it into the branch of its corresponding git repository labeled "RHEL8-Original".  This ensures we have a copy of the original, untouched source from Red Hat to compare any changes to.

**Important:** When we extract this Source RPM, we remove any actual source tarballs from the content.  For example, when extracting the `bash` package, we will remove the bash source tar: bash-4.4.tar.gz.  Why do this?  A few reasons: 

1. We don't want to commit binary/compressed files to git, especially large ones
2. The tarball sources are referred to by upstream URL in the spec file, ex: https://ftp.gnu.org/gnu/bash/bash-%{baseversion}.tar.gz
3. This emulates the CentOS build methodology, which keeps binary artifacts and text files as separate entities.  Coincidentally, it's also a best-practice in source control systems.

<br />

### Step 3: Copy source to "Rocky" Branch
We will maintain another branch in the git project called something like "Rocky8".  This will form the basis for the RockyLinux Source RPM package.

This is when any de-branding or patching steps happen, if necessary.  For many packages, the RHEL8-Original and Rocky8 branches might be identical, or very close.  Some packages will need changes/patches in this branch due to trademark removal, copyright issues, etc.

We hope to automate or script the differences between branches as much as possible, so when a new source comes in, it can be automatically trademark-replaced and committed over to the Rocky8 branch ASAP.

It goes without saying that we'll likely want to tag these branches in Git as new versions are committed, to help keep track of everything.


<br />

### Step 4: Produce a Rocky Linux Source RPM
Now that we have the contents in our own "Rocky8" git branch, we're ready to produce!  An automated build system (likely Koji + Mock) will take the "Rocky8" branch of the project and produce a new Rocky Linux Source RPM.  The build system will auto-download the upstream source tarball referenced in the RPM spec file (ex: https://ftp.gnu.org/gnu/bash/bash-%{baseversion}.tar.gz).

The source tarball (bash-4.4.tar.gz) plus the RPM specfile (bash.spec) plus any RedHat or Rocky-specific patches (ex: bash-4.4.1.patch) means we have enough to build a source RPM.

<br />

### Step 5: Produce a Rocky Linux Binary RPM
This is pretty straightforward.  Once we have a valid source RPM, we use our build system to extract, compile, and produce a valid binary RPM.  We're ready to test, sign, and send it off to the official repository (and mirrors!).


<br />

### Step 6: Sign and Test
RPMs produced by us should be cryptographically signed with a Rocky Linux key, which guarantees to users that the package was indeed built by the Rocky Linux project.

The package will also need to be put through some testing - preferably automated.  The nature of the testing is yet to be determined, but we'll want to do some sanity checks at the bare minimum before unleashing it on the world.  (Is this package installable?  Did we accidentally miss any files?  Does it cause dnf/yum dependency conflicts? etc.)

<br />

### Step 7: Deploy to Repository
Once a package is complete and tested, it will be uploaded to the Rocky Linux repository, and picked up/cloned by a network of repository mirrors around the world.  The Rocky 8 source RPM will of course be uploaded as well.

Users of the distro will then see it when they `dnf update` or `dnf install` !


<br />

### Closing Note:
There is much work to be done, and spelling out what needs to be done is much easier than actually doing it.  I thought we needed some development end-goals, and these steps are what I came up with.  As best I can tell, similar projects (like CentOS) perform all these steps in some manner, even if they aren't published or widely known.

This document is a rough (real rough) draft - feel free to suggest improvements, or ask questions about these goals to the budding rocky-devel group.  We're on Slack and IRC often!

Thanks, -Skip G.  (skip77 on IRC)
