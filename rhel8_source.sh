#!/bin/bash

# Simple script to download the RHEL 8 sources
# (this script assumes you are on a RHEL 8 machine with a valid subscription)

# The source folders (that hold the RPMs) are created in the current working directory - make sure you have some space!


mkdir -p {baseos,supplementary,appstream,codeready}

reposync -v --download-path=./baseos --norepopath --source --remote-time --repoid=rhel-8-for-x86_64-baseos-source-rpms  
reposync -v --download-path=./supplementary --norepopath --source --remote-time --repoid=rhel-8-for-x86_64-supplementary-source-rpms
reposync -v --download-path=./appstream --norepopath --source --remote-time --repoid=rhel-8-for-x86_64-appstream-source-rpms
reposync -v --download-path=./codeready --norepopath --source --remote-time --repoid=codeready-builder-for-rhel-8-x86_64-source-rpms

