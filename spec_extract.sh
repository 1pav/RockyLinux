#!/bin/bash

# Simple script to extract only .spec files from each source rpm in a given folder and subfolders
# Spec files are written to current directory - careful where you launch this from!
# Give the folder holding the RPMs as first argument to this script


FOLDER=$1

for i in `find "${FOLDER}" -iname *.rpm`; do
  SPEC=`rpm -qpl ${i} | grep '\.spec$' | head -1`
  
  rpm2cpio ${i} | cpio -idmv ${SPEC}

done
